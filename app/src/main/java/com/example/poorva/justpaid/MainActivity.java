package com.example.poorva.justpaid;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MainActivity extends Activity {

    public SharedPreferences sharedPref;
    public SharedPreferences.Editor editor;

    String uname1;
    String email1;
    String mobno1;
    String password1;
    String repassword1;
    private static String url = "http://52.74.154.150:3000/api/auth/signup";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

    }


    public void confirm(View v)
    {
        EditText uname = (EditText) findViewById(R.id.editText);
        EditText email = (EditText) findViewById(R.id.editText2);
        EditText mobno = (EditText) findViewById(R.id.editText3);
        EditText password = (EditText) findViewById(R.id.password);
        EditText repassword = (EditText) findViewById(R.id.confirmpass);
        int f =1;

        uname1 = uname.getText().toString();
        email1 = email.getText().toString();
        mobno1 = mobno.getText().toString();
        password1 = password.getText().toString();
        repassword1 = repassword.getText().toString();

        //onBackPressed();

        if((uname1.equals(""))||(email1.equals(""))||(mobno1.equals(""))||(password1.equals(""))||(repassword1.equals("")))
        {
            Toast.makeText(getApplicationContext(), "Enter all fields", Toast.LENGTH_SHORT).show();
        }

        else
        {
            if(!(Patterns.EMAIL_ADDRESS.matcher(email1).matches()))
            {
                f = 0;
                Toast.makeText(getApplicationContext(),"Invalid email id",Toast.LENGTH_SHORT).show();
            }
            if(!(password1.equals(repassword1)))
            {
                f = 0;
                Toast.makeText(getApplicationContext(),"Passwords don't match",Toast.LENGTH_SHORT).show();
            }
            else if (f==1)
            {
                JSONObject data = new JSONObject();

                try {
                    data.put("username", uname1);
                    data.put("email", email1);
                    data.put("mobno", mobno1);
                    data.put("password", password1);
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                new Parser().execute();

                /*Intent myIntent = new Intent(v.getContext(),amount.class);
                startActivity(myIntent);*/
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        EditText uname = (EditText) findViewById(R.id.editText);
        EditText email = (EditText) findViewById(R.id.editText2);
        EditText mobno = (EditText) findViewById(R.id.editText3);
        EditText password = (EditText) findViewById(R.id.password);
        EditText repassword = (EditText) findViewById(R.id.confirmpass);

        uname.setText("");
        email.setText("");
        password.setText("");
        repassword.setText("");
        mobno.setText("");
    }

    private class Parser extends AsyncTask<Void, Void, Void> {

        ProgressDialog pDialog;
        Boolean error;
        String errorMsg;
        String token;
        HashMap<String, String> request;

        protected void Parser(Context context) {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Signing Up...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("fullName", uname1));
            params.add(new BasicNameValuePair("password", password1));
            params.add(new BasicNameValuePair("email", email1));
            params.add(new BasicNameValuePair("number", mobno1));

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, params); //Im getting response as a String

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    error = jsonObj.getBoolean("error");
                    errorMsg = jsonObj.getString("message");

                    //hashmap to record response
                    request = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    //request.put("error", error);
                    request.put("errorMsg", errorMsg);

                    if(!error)
                    {
                        token = jsonObj.getString("token");
                        request.put("token", token);

                        Log.d("Token : "," > " + token);
                    }
                    else if(error)
                    {
                        Log.d("Error message : "," > " + errorMsg);
                        Log.d("Error : ",">" + error);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            if (error)
            {
                editor = sharedPref.edit();
                editor.clear();
                editor.commit();
                Toast.makeText(getApplicationContext(),errorMsg, Toast.LENGTH_SHORT);


            }

            else
            {
                editor = sharedPref.edit();
                editor.putString("token",token);
                editor.commit();
                MainActivity.this.startActivity(new Intent(MainActivity.this, amount.class));

            }

        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
