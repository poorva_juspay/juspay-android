package com.example.poorva.justpaid;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class amount extends ActionBarActivity {

    String mobno1;
    String amount1;
    public SharedPreferences sharedPref;
    public SharedPreferences.Editor editor;
    int f = 1;
    //private static String url = "http://52.74.154.150:3000/api/user/transaction";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amount);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

    }

    public void proceed(View v)
    {
        EditText mobno = (EditText) findViewById(R.id.editText6);
        EditText amount = (EditText) findViewById(R.id.editText7);;

        mobno1 = mobno.getText().toString();
        amount1 = amount.getText().toString();

        //onBackPressed();

        if((mobno1.equals(""))||(amount1.equals("")))
        {
            Toast.makeText(getApplicationContext(), "Enter all fields", Toast.LENGTH_SHORT).show();
        }

        else
        {
            JSONObject data = new JSONObject();
            String token = sharedPref.getString("token", null);


            try {
                data.put("number", mobno1);
                data.put("amount", amount1);
                data.put("token",token);
            }

            catch (JSONException e)
            {
                e.printStackTrace();
            }
            new Parser().execute();

                /*Intent myIntent = new Intent(v.getContext(),options.class);
                startActivity(myIntent);*/

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {

        editor = sharedPref.edit();
        editor.clear();
        editor.commit();

        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EditText mobno = (EditText) findViewById(R.id.editText6);
        EditText amount = (EditText) findViewById(R.id.editText7);;
        mobno.setText("");
        amount.setText("");
    }

    private class Parser extends AsyncTask<Void, Void, Void> {

        ProgressDialog pDialog;
        Boolean error;
        String errorMsg;
        String user_id;
        String email;
        String amount;
        String order_id;
        String date;
        String api_key;
        String signature;
        HashMap<String, String> request;
        String myUrl;

        protected void Parser(Context context)
        {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(amount.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();
            String token = sharedPref.getString("token", null);


            List<NameValuePair> params= new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("number",mobno1));
            params.add(new BasicNameValuePair("amount",amount1));
            params.add(new BasicNameValuePair("token",token));

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall("http://52.74.154.150:3000/api/user/transaction", ServiceHandler.POST,params); //Im getting response as a String

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    error = jsonObj.getBoolean("error");
                    errorMsg = jsonObj.getString("message");

                    //hashmap to record response
                    request = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    //request.put("error", error);
                    request.put("errorMsg", errorMsg);

                    if(error)
                    {
                        Toast.makeText(getApplicationContext(),request.get("errorMsg"),Toast.LENGTH_SHORT);
                        editor = sharedPref.edit();
                        editor.clear();
                        editor.commit();
                        finish();
                    }

                    else
                    {
                        user_id = jsonObj.getString("user_id");
                        order_id = jsonObj.getString("order_id");
                        email = jsonObj.getString("email");
                        api_key = jsonObj.getString("api_key");
                        //date = jsonObj.getString("dateTime");
                        signature = jsonObj.getString("signature");

                        request.put("user_id",user_id);
                        request.put("order_id",order_id);
                        request.put("email",email);
                        request.put("api_key",api_key);
                        request.put("signature",signature);

                        Uri.Builder builder = new Uri.Builder();
                        builder.scheme("https")
                                .authority("api.juspay.in")
                                .appendPath("merchant")
                                .appendPath("ipay")
                                .appendQueryParameter("merchant_id", "anmol_juspay")
                                .appendQueryParameter("order_id", order_id)
                                .appendQueryParameter("amount",amount1)
                                .appendQueryParameter("customer_id",user_id)
                                .appendQueryParameter("customer_email",email)
                                .appendQueryParameter("signature",signature);

                        myUrl = builder.build().toString();

                        Log.d("Amount URL",">"+myUrl);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    f = 0;
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            Log.d(" InPOstExecute"," >  :)" );
            if(f==1)
            {
                if (pDialog.isShowing())
                    pDialog.dismiss();

                Log.d(" InPOstExecute"," >  f = 1" );

                Intent i = new Intent(amount.this,options.class);
                i.putExtra("url",myUrl);
                startActivity(i);
            }
            else
            {
                Log.d(" InPOstExecute"," >  f = 0" );
                Intent i = new Intent(amount.this,end.class);
                startActivity(i);

            }
            // Dismiss the progress dialog
             //amount.this.startActivity(new Intent(amount.this, options.class).putExtra("url",myUrl));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            Intent myIntent = new Intent(amount.this,logout.class);
            startActivity(myIntent);
            return true;

        }
        else if(id == R.id.action_history)
        {
            Intent myIntent = new Intent(amount.this,history.class);
            startActivity(myIntent);
        }

        return super.onOptionsItemSelected(item);
    }


}
