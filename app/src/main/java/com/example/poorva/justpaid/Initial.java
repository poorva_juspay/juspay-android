package com.example.poorva.justpaid;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Initial extends Activity {

    public SharedPreferences sharedPref;
    public SharedPreferences.Editor editor;

    public HashMap<String, String> user;

    String email1;
    String password1;
    private static String url = "http://52.74.154.150:3000/api/auth/login";
    int f = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //sharedPref = getApplicationContext().getSharedPreferences("poorva",Context.MODE_PRIVATE);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String str = sharedPref.getString("token",null);
        super.onCreate(savedInstanceState);

        Log.d("initial token value",">" + str);

        //if authtoken, next intent
        if(str != null)
        {
            f = 0;
            Intent myIntent = new Intent(Initial.this,amount.class);
            startActivity(myIntent);
        }
        else {

            setContentView(R.layout.activity_initial);
        }
    }

    public void signup(View v)
    {
        Intent myIntent = new Intent(v.getContext(),MainActivity.class);
        startActivity(myIntent);
    }

    public void signin(View v)
    {
        EditText email =  (EditText) findViewById(R.id.editText4);
        EditText password = (EditText) findViewById(R.id.editText5);
        email1 = email.getText().toString();
        password1 = password.getText().toString();


        if((!password1.equals(""))&& (!email1.equals("")))
        {
            if(Patterns.EMAIL_ADDRESS.matcher(email1).matches())
            {
                JSONObject data = new JSONObject();

                try {
                    data.put("email", email1);
                    data.put("password", password1);
                }

                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                new Parser().execute();

                /*Intent myIntent = new Intent(v.getContext(),amount.class);
                startActivity(myIntent);*/
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Invalid email id",Toast.LENGTH_SHORT).show();
            }
        }
        else
            Toast.makeText(getApplicationContext(),"Enter all fields",Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(f==1)
        {
            EditText email =  (EditText) findViewById(R.id.editText4);
            EditText password = (EditText) findViewById(R.id.editText5);
            email.setText("");
            password.setText("");
        }
    }

    private class Parser extends AsyncTask<Void, Void, Void> {

        ProgressDialog pDialog;
        Boolean error;
        String errorMsg;
        String token;
        HashMap<String, String> request;

        protected void Parser(Context context)
        {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(Initial.this);
            pDialog.setMessage("Logging In...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params= new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("password",password1));
            params.add(new BasicNameValuePair("email",email1));

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST,params); //Im getting response as a String

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    error = jsonObj.getBoolean("error");
                    errorMsg = jsonObj.getString("message");

                    //hashmap to record response
                    request = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    //request.put("error", error);
                    request.put("errorMsg", errorMsg);

                    if(!error)
                    {
                        token = jsonObj.getString("token");
                        request.put("token", token);
                    }

                    else
                    {
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

           if(error)
            {
                Toast.makeText(getApplicationContext(),errorMsg,Toast.LENGTH_SHORT);


                editor = sharedPref.edit();
                editor.clear();
                editor.commit();

            }

            else
            {
                editor = sharedPref.edit();
                editor.putString("token",token);
                editor.commit();
                Initial.this.startActivity(new Intent(Initial.this, amount.class));
            }

        }
  }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
