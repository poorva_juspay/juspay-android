package com.example.poorva.justpaid;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;


public class options extends ActionBarActivity {

    public SharedPreferences sharedPref;
    public SharedPreferences.Editor editor;
    String token;
    private WebView webView;
    String url;

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        url = getIntent().getStringExtra("url");
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token =  sharedPref.getString("token", null);


        Log.d(" Entered Webview", " > :D");

        webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebContentsDebuggingEnabled(true);
        webView.loadUrl(url); //***********the website
        Log.d(" Url is ", " >" +url);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if(url.contains("google.com")){
                    Toast.makeText(getApplicationContext(),"Your payment was successful",Toast.LENGTH_LONG).show();

                }else if(url.contains("facebook.com")){
                    Toast.makeText(getApplicationContext(),"Your payment was Unsuccessful",Toast.LENGTH_LONG).show();
                    Intent i = new Intent(options.this, end.class);
                    startActivity(i);
                    finish();
                }
                super.onPageStarted(view, url, favicon);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        editor = sharedPref.edit();
        editor.clear();
        editor.commit();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent myIntent = new Intent(options.this,logout.class);
            startActivity(myIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
