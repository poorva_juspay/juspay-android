package com.example.poorva.justpaid;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by poorva on 15/5/15.
 */
public class logout extends Activity{

    private static String url = "http://52.74.154.150:3000/api/auth/login";
    int flag = 1;
    public SharedPreferences sharedPref;
    public SharedPreferences.Editor editor;
    public String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        token = sharedPref.getString("token",null);

        new Logger().execute();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private class Logger extends AsyncTask<Void, Void, Void> {

        ProgressDialog pDialog;
        Boolean error;
        String errorMsg;
        String token;
        HashMap<String, String> request;

        protected void Logger(Context context)
        {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(logout.this);
            pDialog.setMessage("Logging Out...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance

            ServiceHandler sh = new ServiceHandler();
            List<NameValuePair> logout= new ArrayList<NameValuePair>();
            logout.add(new BasicNameValuePair("token",token));
            Log.d("   TOken Logout :", " >" + token);
            String jsonStr = sh.makeServiceCall("http://52.74.154.150:3000/api/auth/logout", ServiceHandler.GET,logout);

            if(jsonStr == null)
            {
                flag = 0;
            }


            Log.d("Logout msg : ",">" + jsonStr);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            if(flag == 1)
            {
                editor = sharedPref.edit();
                editor.putString("token",token);
                editor.commit();
                onBackPressed();
                Intent myIntent = new Intent(logout.this,Initial.class);
                startActivity(myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            }

        }
    }


}
