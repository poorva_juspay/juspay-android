package com.example.poorva.justpaid;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class history extends ListActivity {

    public SharedPreferences sharedPref;
    public SharedPreferences.Editor editor;

    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ListView lv = getListView();
        new previous().execute();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private class previous extends AsyncTask<Void, Void, Void> {

        ProgressDialog pDialog;
        Boolean error;
        String message;
        String user_id;
        String email;
        String amount;
        String _id;
        String date;
        String number;

        HashMap<String, String> request;
        ArrayList<HashMap<String, String>> historyList = new ArrayList<HashMap<String, String>>();
        JSONArray previousHistory = null;
        JSONObject errorCheck = null;
        JSONObject errorMsg;


        protected void previous(Context context)
        {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(history.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            // Creating service handler class instance
            token = sharedPref.getString("token", null);
            ServiceHandler sh = new ServiceHandler();

            Log.d("Token : ", ">"+token);

            List<NameValuePair> history= new ArrayList<NameValuePair>();
            history.add(new BasicNameValuePair("token",token));

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall("http://52.74.154.150:3000/api/user/transaction", ServiceHandler.GET,history); //Im getting response as a String

            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {

                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    previousHistory = jsonObj.getJSONArray("transactions");
                    //errorCheck = jsonObj.getJSONObject("error");
                    //errorMsg = jsonObj.getJSONObject("message");

                    error = jsonObj.getBoolean("error");
                    message = jsonObj.getString("message");

                    Log.d(" History message : ","> "+message);

                    // looping through All Contacts
                    for (int i = 0; i < previousHistory.length(); i++)
                    {
                        JSONObject c = previousHistory.getJSONObject(i);

                        //hashmap to record response
                        request = new HashMap<String, String>();

                        if(error)
                        {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
                            editor = sharedPref.edit();
                            editor.clear();
                            editor.commit();
                            finish();
                        }

                        else
                        {
                            user_id = c.getString("user_id");
                            _id = c.getString("_id");
                            amount = c.getString("amount");
                            date = c.getString("dateTime");
                            number = c.getString("number");

                            request.put("user_id",user_id);
                            request.put("order_id",_id);
                            request.put("amount",amount);
                            request.put("number",number);
                            request.put("date",date);

                            historyList.add(request);

                            Log.d("  received String : "," number = " + number + " date = " + date + "Amount = " + amount);

                        }
                    }

                }
                catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();


            ListAdapter adapter = new SimpleAdapter(
                    history.this, historyList,
                    R.layout.list_item, new String[] { "number", "date",
                    "amount" }, new int[] { R.id.mobile,
                    R.id.date, R.id.amount });


            Log.d("  List adapter : "," number = " + number + " date = " + date + "Amount = " + amount);

            setListAdapter(adapter);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            Intent myIntent = new Intent(history.this,logout.class);
            startActivity(myIntent);
            return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
